let trainer = {
  fullName: "Ash Ketchum",
  age: 10,
  pokemon: ["Pikachu", "Bulbasaur", "Squirtle", "Eevee"],
  friends: {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"],
  },
};

console.log("Result of dot notation:");
console.log(trainer.age);

console.log("Result of bracket notation");
console.log(trainer["pokemon"]);

trainer.talk = function () {
  console.log("Pikachu! I choose you!");
};

trainer.talk();

function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = level * 5;
  this.attack = level * 2;
  this.tackle = function (target) {
    console.log(`${this.name} tackled ${target.name}`);
    let targetPokemonHealth = target.health - this.attack;
    if (targetPokemonHealth <= 0) {
      this.faint(target);
    } else {
      console.log(`${target.name}'s health is now ${targetPokemonHealth}'`);
    }
    return (target.health = targetPokemonHealth);
  };
  this.faint = function (target) {
    console.log(`${target.name} has fainted!`);
  };
}

let pikachu = new Pokemon("Pikachu", 10);
let eevee = new Pokemon("Eevee", 12);
let bulbasaur = new Pokemon("Bulbasaur", 7);

console.log(pikachu);
console.log(eevee);
console.log(bulbasaur);

pikachu.tackle(eevee);
pikachu.tackle(eevee);
bulbasaur.tackle(eevee);
bulbasaur.tackle(eevee);
console.log(eevee);
