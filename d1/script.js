let grades = [98.5, 94.3, 89.2, 90.1];
// can access arrays with the index
console.log(grades[0]);
/* 
  Limitations of arrays 
  - no context on the data 
*/

/* 
  An object is similar to an array 
  - its a collection of related data and or functionalities.
  - usually represents real world object 
  - syntax: 
      - let objectName = {
        keyA: valueA,
        keyB: valueB
      }
  - has a key value pair = literal notion 
    - object initializer/literal notion 
      - describes/initializes an object 
      - an object consists of properties which ar used to describe an object 
  - how to access the data in an object
    - dot notation 
        array.key
    - bracket notation
        array[key] - can be confusing so dot is better practice 

*/

let grade = {
  math: 89.2,
  english: 98.5,
  science: 90.1,
  filipino: 94.3,
};
// console.log(grade);
console.log(grade.english);

let cellphone = {
  brandName: "Nokia 3310",
  color: "Dark Blue",
  manufactureDate: 1999,
};

console.log(typeof cellphone);

let student = {
  firstName: "John",
  lastName: "Smith",
  mobieNumber: 091234567890,
  location: {
    //object inside object
    city: "Tokyo",
    country: "Japan",
  },
  emails: ["john@mail.com", "johnsmith@mail.xyz"],
  //function in object = object method
  fullName: function () {
    return this.firstName + " " + this.lastName;
  },
};

//accessing object inside object
console.log(student.location.city);

//bracket notation
console.log(student["firstName"]);

//arrays in objects
console.log(student.emails[0]);

//functions in objects = object method
console.log(student.fullName());

/* 
  This keyword 
  - indicates the owner of the function 
  - indicates the current object it is located in 
*/
const person1 = {
  name: "Jane",
  greeting: function () {
    return `Hi I'm ${this.name}`;
  },
};

console.log(person1.greeting());

//array of objects
let contactList = [
  {
    firstName: "John",
    lastName: "Smith",
    location: "Japan",
  },
  {
    firstName: "Jane",
    lastName: "Smith",
    location: "Japan",
  },
  {
    firstName: "Jasmin",
    lastName: "Smith",
    location: "Japan",
  },
];

console.log(contactList[0].firstName);

let people = [
  {
    name: "Juanita",
    age: 13,
  },
  {
    name: "Juanito",
    age: 14,
  },
];

people.forEach(function (person) {
  console.log(person.name);
});

// creating objects using a constructor function (JS object constructions/object from blueprints)
/* 
  creates a reusable function to create several objects that have the same data structure 

  useful for creating multiple copies/instances of an object 

  object literals 
    let object = {}
  
  instance - has a distinct/unique object
    - concrete occurence of any object which emphasizes on the unique identity of it 
    - reusable function 
    let object = new object 
    syntax: 
      function objectName(keyA, keyB){
        this.keyA = keyA,
        this.keyB = keyB
      }
*/

function Laptop(name, manufactureDate) {
  //the this keyword allows to assign a new object's property by associating them with values received from our parameter
  (this.name = name), (this.manufactureDate = manufactureDate);
}
//this is a unique instance of the laptop object

let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using object constructors");
console.log(laptop);

//This is another unique instance of the laptop object
let myLaptop = new Laptop("Macbook Air", 2020);
//without new keyword will return undefined - function will just be invoked but will not create a new object
console.log("Result from creating objects using object constructors");
console.log(myLaptop);

//creating empty objects - 2 ways
let computer = {};
let myComputer = new Object();

console.log(myLaptop.name);
console.log(myLaptop["name"]);

let array = [laptop, myLaptop];
console.log(array[0].name);

//initializing/adding/deleting/reassigning object properties

//initialized/added properties after the object was created - useful for when an object's properties are undetermined at the time of creation
let car = {};

car.name = "Honda Civic";
console.log(car);

car["manufactureDate"] = 2019;
console.log(car);

// reassigning object properties
car.name = "Honda Accord"; // will replace the car name
console.log(car);

//deleting object properties
delete car["manufactureDate"]; // can use dot notation
console.log(car);

/* 
  Object Methods 
  - a method is a function which is a property of an object 
  - also functions and one of the key differences they have is mthods are functions related to a specific object 
*/
let person = {
  name: "John",
  talk: function () {
    console.log("Hello my name is " + this.name);
  },
};
console.log(person);
//using the object method
console.log("Result from object methods");
person.talk();

//adding methods to object person
person.walk = function () {
  console.log(this.name + " walked 25 steps forward");
};
person.walk();

let friend = {
  firstName: "Joe",
  lastName: "Smith",
  address: {
    city: "Austin",
    state: "Texas",
  },
  emails: ["joe@mail.com", "joesmith@mail.xyz"],
  introduce: function () {
    console.log(`Hello my name is ${this.firstName} ${this.lastName}`);
  },
};
friend.introduce();
/* 
  real world application 
  - Scenario
    1. create a game that would have several pokemon interact with each other 
    2. every pokemon would have the same set of stats, properties, and functions 
*/

//using object literals to create multiple kinds of pokemon would be time consuming
let myPokemon = {
  name: "Pikachu",
  level: 3,
  health: 100,
  attack: 50,
  tackle: function () {
    console.log("This pokemon tackled target pokemon");
    console.log(
      "'targetPokemon's health is now reduced to _targetPokemonHealth_'"
    );
  },
  faint: function () {
    console.log("Pokemon fainted ");
  },
};
//create an object constructor instead of object literals to save time
function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = level;
  //method
  this.tackle = function (target) {
    console.log(`${this.name} tackled ${target.name}`);
    console.log(
      "targetPokemon's health is now reduced to _targetPokemonHealth_"
    );
  };
  this.faint = function () {
    console.log(`${this.name} fainted`);
  };
}

let pikachu = new Pokemon("Pikachu", 16);
let charizard = new Pokemon("Charizard", 8);
pikachu.tackle(charizard);
